# Router

   https://www.ciscopress.com/articles/article.asp?p=2180210&seqNum=4	

   Explains multiple routing protocols and describes their relative strengths and weaknesses. It also shows how to read a routing table easily and interpret the IPv6 routing information listed within it.

   https://www.inetdaemon.com/tutorials/internet/ip/routing/define_router.shtml

   Basic information about a router

# Subnet

   [Sunny Classroom  - Subnetting Simple](https://www.youtube.com/watch?v=ecCuyq-Wprc)
	
   Basic information about subnetting
   
   https://www.aelius.com/njh/subnet_sheet.html

   Cheatsheet

# IP-address 

   https://tools.ietf.org/html/rfc8200
	
   Internet protocol v6 standards


   https://computer.howstuffworks.com/internet/basics/what-is-an-ip-address.htm

   Basic make up of ip-addresses 

# Network
	
   https://www.ibm.com/cloud/learn/networking-a-complete-guide
   
   Introduction guide to networking
	
   [Geek Lessons - Computer Networking Course, Beginner to Advance](https://www.youtube.com/watch?v=QKfk7YFILws)
   
   A FULL course on network

# Mac addresses:

   https://www.pcmag.com/encyclopedia/term/mac-address
	
   Pretty simple explanation of mac addresses and what they are.

   
   https://standards.ieee.org/content/dam/ieee-standards/standards/web/documents/tutorials/macgrp.pdf 
   
   Complicated but deep explanation of the conventions for how mac addresses are assigned and what they are in general.

# Switches:

   https://www.cisco.com/c/en/us/solutions/small-business/resource-center/networking/network-switch-vs-router.html
   
   Easy layman description but biased by being written by a company.

   https://www.cloudflare.com/learning/network-layer/what-is-a-network-switch/
   
   More in depth but relatively easy to understand, still colored by a company’s biases.

# OSI layers:

   http://www.danskdatasikkerhed.dk/wp-content/uploads/2016/06/OSI-Model-opgave.docx 
   
   It explains in depth how  it works and what it’s funktion is. it’s not an easy read guide, you're gonna have to spend some time on it.  

   https://www.geeksforgeeks.org/layers-of-osi-model/
   
   It clear on what it want to explain and are easily read 

# Netmask: 

   https://www.hacksplaining.com/glossary/netmasks
   
   Quick and easily read guide to subnet masks, very barebones but still goes into enough detail for a quick understanding of the subject.
